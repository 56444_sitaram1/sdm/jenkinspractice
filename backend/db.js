const mysql = require('mysql2')

const pool = mysql.createPool({
 
  host: 'mydb',

  user: 'root',
  password: 'root',
  database: 'mydb1',
  port: 3306,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
})

module.exports = pool
