const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.post('/add', (request, response) => {
  const {EmpId, Name, Salary } = request.body
console.log(request.body);
  const query = `
    INSERT INTO Emp
      (EmpId, Name, Salary)
    VALUES
      ('${EmpId}', ${Name}, '${Salary}')
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.get('/findById/:EmpId', (request, response) => {
  const query = `
    SELECT EmpId, Name, Salary
    FROM Emp
    WHERE
      EmpId = ${EmpId}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.put('/update/:EmpId', (request, response) => {
  const { EmpId } = request.params
  const { Name, Salary } = request.body

  const query = `
    UPDATE Emp
    SET
      Name = '${Name}', 
      Salary = ${Salary}, 
      
    WHERE
    EmpId = ${EmpId}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.delete('/removeById/:EmpId', (request, response) => {
  const { EmpId } = request.params

  const query = `
    DELETE FROM Emp
    WHERE
    EmpId = ${EmpId}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

module.exports = router

